package fo.nya.punch.teavm;

import com.github.xpenatan.gdx.backends.teavm.TeaBuildConfiguration;
import com.github.xpenatan.gdx.backends.teavm.TeaBuilder;
import com.github.xpenatan.gdx.backends.teavm.plugins.TeaReflectionSupplier;
import com.github.xpenatan.gdx.backends.web.gen.SkipClass;
import fo.nya.pepperoni.entity.Bullet;
import fo.nya.pepperoni.entity.Coin;
import fo.nya.pepperoni.entity.Mob;
import org.teavm.tooling.TeaVMTool;

import java.io.File;
import java.io.IOException;

/**
 * Builds the TeaVM/HTML application.
 */
@SkipClass
public class TeaVMBuilder {
    public static void main(String[] args) throws IOException {
        TeaBuildConfiguration teaBuildConfiguration = new TeaBuildConfiguration();
        teaBuildConfiguration.assetsPath.add(new File("../assets"));
        teaBuildConfiguration.webappPath = new File("build/dist").getCanonicalPath();
        // You can switch this setting during development:
        teaBuildConfiguration.obfuscate = true;

        // Register any extra classpath assets here:
        // teaBuildConfiguration.additionalAssetsClasspathFiles.add("fo/nya/.../asset.extension");

        // Register any classes or packages that require reflection here:
        // TeaReflectionSupplier.addReflectionClass("fo.nya.[...].reflect");
        TeaReflectionSupplier.addReflectionClass(Mob.class);
        TeaReflectionSupplier.addReflectionClass(Bullet.class);
        TeaReflectionSupplier.addReflectionClass(Coin.class);


        TeaVMTool tool = TeaBuilder.config(teaBuildConfiguration);
        tool.setMainClass(TeaVMLauncher.class.getName());
        TeaBuilder.build(tool);
    }
}
