package fo.nya.pepperoni;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import fo.nya.pepperoni.entity.Spawn;

/**
 * Класс содержащий в себе настройки одной 'волны'.
 */
public class Wave {

    /**
     * Класс с настройками для конкретной точки.
     */
    private static class SpawnSettings {
        /**
         * Скорость воспроизведения врагов.
         * <br><i>{@code 0} - мгновенное воспроизведение, 1 - одна секунда и т.д.</i>
         */
        float rate;

        /**
         * Очередь врагов для воспроизведения.
         */
        Array<MobPrototype> prototypes;
    }

    /**
     * Коллекция с настройками для разных точек в этой волне.
     */
    private final ObjectMap<Integer, SpawnSettings> settings = new ObjectMap<>();

    /**
     * Метод для настройки волны, устанавливает скорость воспроизведения врагов для определенной точки.
     *
     * @param id   id точки которую нужно настроить.
     * @param rate скорость воспроизведения врагов.
     */
    public void setRate(int id, float rate) {
        computeIfAbsent(id).rate = rate;
    }

    /**
     * Метод для настройки волны, устанавливает список врагов для определенной точки.
     *
     * @param id         id точки которую нужно настроить.
     * @param prototypes список врагов.
     */
    public void setPrototypes(int id, Array<MobPrototype> prototypes) {
        computeIfAbsent(id).prototypes = prototypes;
    }

    /**
     * Метод достает из {@link #settings} настройки для конкретной точки,
     * либо если настроек для этой точки еще, то создает новый объект настроек,
     * добавляет его в коллекцию и возвращает его же.
     *
     * @param id id точки, настройки для которой нужно получить.
     * @return настройки конкретной точки.
     */
    private SpawnSettings computeIfAbsent(int id) {
        SpawnSettings s = settings.get(id);

        if (s == null) {
            s = new SpawnSettings();
            settings.put(id, s);
        }

        return s;
    }

    /**
     * Настроить определенную точку.
     *
     * @param spawn точка которую нужно настроить.
     * @return количество врагов которое было добавлено в очередь.
     */
    public int setup(Spawn spawn) {

        SpawnSettings s = settings.get(spawn.getId());
        if (s == null || s.rate == 0 || s.prototypes == null) return 0;

        spawn.setup(s.rate, s.prototypes);
        return s.prototypes.size;
    }
}
