package fo.nya.pepperoni;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.*;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import fo.nya.pepperoni.entity.*;
import fo.nya.pepperoni.island.Island;
import fo.nya.pepperoni.island.IslandPoint;
import fo.nya.pepperoni.util.AnimatedDrawable;
import fo.nya.pepperoni.util.Ui;
import fo.nya.pepperoni.util.WorldCoords;
import lombok.Getter;

/**
 * Основной класс и экран игры, по сути является типичным страшным 'Гейм Менеджером'.
 */
public class Gameplay extends ScreenAdapter {

    /**
     * Пул объектов {@link Bullet}.
     */
    private final static Pool<Bullet> BULLET_POOL = Pools.get(Bullet.class);

    /**
     * Пул объектов {@link Mob}.
     */
    private final static Pool<Mob> MOB_POOL = Pools.get(Mob.class);

    /**
     * Пул объектов {@link Coin}.
     */
    private final static Pool<Coin> COIN_POOL = Pools.get(Coin.class);

    /**
     * Стоимость одной башни.
     */
    private final static int TOWER_COST = 10;

    /**
     * Ссылка на класс игры.
     */
    private final PepperoniDefense game;

    /**
     * Основной скин UI.
     */
    private final Skin skin;

    /**
     * Сцена UI
     */
    private final Stage stage;

    /**
     * Viewport для игры.
     */
    private final Viewport viewport;

    /**
     * SpriteBatch для отрисовки игры.
     */
    private final SpriteBatch batch;

    /**
     * ShapeRenderer для отладки всех волшебных переменных. Кнопка {@code D}.
     */
    private final ShapeRenderer shapeRenderer;

    /**
     * Кеш вектора для перевода координат мыши из Viewport'а.
     */
    private final Vector2 unProjectMouse = new Vector2();

    /**
     * Карта на которой происходит игра.
     */
    @Getter private final Island island;

    /**
     * Флаг оконченной игры.
     */
    @Getter private boolean gameOver = false;

    /**
     * Флаг паузы.
     */
    private boolean pause = false;

    /**
     * 'Глобальный' таймер анимации.
     */
    @Getter private float animationStep = 0;

    /**
     * Текущее кол-во монеток.
     */
    private int coinsNow = 50;

    /**
     * Кол-во побежденных врагов за текущую волну.
     */
    private int killsInWave;

    /**
     * Кол-во врагов в текущей волне.
     */
    private int totalMobsInWave;

    /**
     * Кол-во выставленных врагов в текущей волне.
     */
    private int spawnedMobsInWave;

    /**
     * Номер волны, где {@code 0} - первая волна.
     * <br> {@code -2} -> вывести сообщение о том что нужно потратить все деньги.
     * <br> {@code -1} -> ожидание траты денег.
     */
    private int wave = -2;

    /**
     * Флаг того что волна закончилась.
     */
    private boolean waveIsEnded = true;

    /**
     * Список всех активных сущностей.
     */
    @Getter private final Array<Entity> entities = new Array<>();

    /**
     * Список всех активных частичек.
     */
    private final Array<ParticleEffectPool.PooledEffect> effects = new Array<>();

    /**
     * Пул частичек для огурца.
     */
    private final ParticleEffectPool cucumberExplosionPool;

    /**
     * Прогресс волны.
     */
    private final ProgressBar progressBar;

    /**
     * Текст для отображения кол-ва монеток.
     */
    private final Label coinsDisplay;

    /**
     * Буквы которые нужно вывести в диалоге.
     */
    private final Queue<Character> speech = new Queue<>();

    /**
     * Текст диалога.
     */
    private final Label speechLabel;

    /**
     * Анимация банки с огурцами.
     */
    public final Animation<TextureRegion> cucumberJarAnimation;

    /**
     * Анимация воды.
     */
    public final Animation<TextureRegion> waterAnimation;

    /**
     * Анимация выпадающей монетки.
     */
    public final Animation<TextureRegion> coinAnimation;

    /**
     * Картинки земли.
     */
    public final Array<? extends TextureRegion> groundTextures;

    /**
     * Картинки дорожки.
     */
    public final Array<? extends TextureRegion> pathTextures;

    /**
     * Картинки моста.
     */
    public final Array<? extends TextureRegion> bridgeTextures;

    /**
     * Картинки пепперони.
     */
    public final Array<? extends TextureRegion> pepperoniTextures;

    /**
     * Картинки с наземными декорациями.
     */
    public final Array<? extends TextureRegion> groundDecorationTextures;

    /**
     * Картинки с водными декорациями.
     */
    public final Array<? extends TextureRegion> waterDecorationTextures;

    /**
     * Картинка сердечка.
     */
    public final TextureRegion heartTexture;

    /**
     * Картинка огурца.
     */
    public final TextureRegion cucumberTexture;

    /**
     * Картинка домика котов.
     */
    public final TextureRegion catSpawnTexture;

    /**
     * Картинка места куда можно ставить башню.
     */
    public final TextureRegion towerSpotTexture;

    /**
     * Картинка фронтовой стороны рогатки.
     */
    public final TextureRegion slingshotFrontTexture;

    /**
     * Картинка задней стороны рогатки.
     */
    public final TextureRegion slingshotBackTexture;

    /**
     * Картинка монетки для отображения в баре.
     */
    public final TextureRegion coinDisplayTexture;

    /**
     * Картинка 'паузы'.
     */
    public final TextureRegion burgerTexture;

    /**
     * Анимации передвижения котов.
     */
    public final ObjectMap<String, Animation<TextureRegion>> catWalkAnimation = new ObjectMap<>();

    public Gameplay(PepperoniDefense game, AssetManager assets, Island island) {
        this.game = game;

        this.island = island;

        final float width = WorldCoords.toScreenX(island.getWidth());
        final float height = WorldCoords.toScreenY(island.getHeight());

        viewport = new ExtendViewport(width, height);

        viewport.getCamera().position.x = width / 2;
        viewport.getCamera().position.y = height / 2;

        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();

        // Загрузка текстур и анимаций

        TextureAtlas atlas = Assets.ATLAS.getFrom(assets);

        cucumberJarAnimation = new Animation<>(0.6f, atlas.findRegions("cucumber_jar"), Animation.PlayMode.LOOP);
        waterAnimation = new Animation<>(0.25f, atlas.findRegions("water"), Animation.PlayMode.LOOP);
        coinAnimation = new Animation<>(0.15f, atlas.findRegions("coin"), Animation.PlayMode.LOOP);

        groundTextures = atlas.findRegions("ground");
        pathTextures = atlas.findRegions("path");
        bridgeTextures = atlas.findRegions("bridge");
        pepperoniTextures = atlas.findRegions("pepperoni");
        groundDecorationTextures = atlas.findRegions("ground_decoration");
        waterDecorationTextures = atlas.findRegions("water_decoration");

        heartTexture = atlas.findRegion("heart");
        cucumberTexture = atlas.findRegion("cucumber");
        catSpawnTexture = atlas.findRegion("cat_spawn");
        towerSpotTexture = atlas.findRegion("tower_spot");
        slingshotFrontTexture = atlas.findRegion("slingshot_front");
        slingshotBackTexture = atlas.findRegion("slingshot_back");
        coinDisplayTexture = atlas.findRegion("coin_display");
        burgerTexture = atlas.findRegion("burger");

        catWalkAnimation.put("gray_cat_down", new Animation<>(MobPrototype.GRAY.animationSpeed, atlas.findRegions("gray_cat_down"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("gray_cat_up", new Animation<>(MobPrototype.GRAY.animationSpeed, atlas.findRegions("gray_cat_up"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("gray_cat_left", new Animation<>(MobPrototype.GRAY.animationSpeed, atlas.findRegions("gray_cat_left"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("gray_cat_right", new Animation<>(MobPrototype.GRAY.animationSpeed, atlas.findRegions("gray_cat_right"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("ginger_cat_down", new Animation<>(MobPrototype.GINGER.animationSpeed, atlas.findRegions("ginger_cat_down"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("ginger_cat_up", new Animation<>(MobPrototype.GINGER.animationSpeed, atlas.findRegions("ginger_cat_up"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("ginger_cat_left", new Animation<>(MobPrototype.GINGER.animationSpeed, atlas.findRegions("ginger_cat_left"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("ginger_cat_right", new Animation<>(MobPrototype.GINGER.animationSpeed, atlas.findRegions("ginger_cat_right"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("purple_cat_down", new Animation<>(MobPrototype.PURPLE.animationSpeed, atlas.findRegions("purple_cat_down"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("purple_cat_up", new Animation<>(MobPrototype.PURPLE.animationSpeed, atlas.findRegions("purple_cat_up"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("purple_cat_left", new Animation<>(MobPrototype.PURPLE.animationSpeed, atlas.findRegions("purple_cat_left"), Animation.PlayMode.LOOP));
        catWalkAnimation.put("purple_cat_right", new Animation<>(MobPrototype.PURPLE.animationSpeed, atlas.findRegions("purple_cat_right"), Animation.PlayMode.LOOP));

        cucumberExplosionPool = new ParticleEffectPool(Assets.CUCUMBER_EXPLOSION.getFrom(assets), 5, 25);

        // Установка сущностей с острова:
        island.setup(this);


        // Настройка UI

        skin = Assets.SKIN.getFrom(assets);
        stage = new Stage(new ExtendViewport(width, height));
        Gdx.input.setInputProcessor(stage);

        for (Texture texture : skin.getAtlas().getTextures()) {
            texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        }

        // Настройка UI: основная таблица
        Table table = new Table();

        table.top();
        table.padTop(5);
        table.setFillParent(true);

        stage.addActor(table);

        // Настройка UI: прогресс волны
        progressBar = new ProgressBar(0, 1, 0.001f, false, skin);
        table.add(progressBar).width(Value.percentWidth(0.5f, table));

        // Настройка UI: плашка с монетами.
        Window coinsDisplayPlate = new Window("", skin);
        coinsDisplayPlate.add(new Image(coinDisplayTexture));
        coinsDisplayPlate.getTitleTable().setVisible(false);
        coinsDisplay = new Label("", skin);
        coinsDisplayPlate.add(coinsDisplay);
        table.add(coinsDisplayPlate).width(Value.percentWidth(0.15f, table));

        //  // Настройка UI: кнопка паузы

        Button button = new Button(skin);
        button.add(new Image(burgerTexture));
        button.addListener(new ChangeListener() {
            @Override public void changed(ChangeEvent event, Actor actor) {pause();}
        });
        table.add(button);

        // Настройка UI: вторая строчка с говорящим котом.

        table.row().expandY();

        // Window dialog = new Window("", from);
        Table dialog = new Table();
        dialog.pad(10);

        // Окно с текстом
        Window speech = new Window("", skin, "speech");
        speechLabel = new Label("Meow!", skin, "dark");
        speech.add(speechLabel).padTop(10).padLeft(5).padRight(5);
        dialog.add(speech);

        // Аватарка кота
        Window photo = new Window("", skin, "photo");
        photo.add(new Image(new AnimatedDrawable(new Animation<>(0.3f, atlas.findRegions("dialog_cat"), Animation.PlayMode.LOOP))));
        dialog.add(photo);

        table.add(dialog).colspan(3).bottom().right();
    }

    @Override public void render(float delta) {

        // Обновляем монетки в шапке.
        coinsDisplay.setText(" × " + coinsNow);

        update(delta);

        draw(delta);

    }

    @Override public void resize(int width, int height) {
        viewport.update(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override public void pause() {
        Gameplay.this.pause = true;

        Ui.addPopUpWindow(stage, skin, "Pause",
                window -> {

                    window.add(Ui.button("Resume", skin, (__, ___) -> window.addAction(
                            Actions.sequence(
                                    Actions.fadeOut(0.2f),
                                    Actions.parallel(
                                            Actions.removeActor(window.getParent()),
                                            Actions.run(() -> Gameplay.this.pause = false)))))).grow();

                    window.row();

                    window.add(Ui.button("Restart", skin, (__, ___) -> game.reset())).grow();
                });
    }

    @Override public void dispose() {
        batch.dispose();
        shapeRenderer.dispose();
        stage.dispose();

        for (Entity entity : entities) {
            if (entity instanceof Bullet) {
                BULLET_POOL.free((Bullet) entity);
            } else if (entity instanceof Mob) {
                MOB_POOL.free((Mob) entity);
            } else if (entity instanceof Coin) {
                COIN_POOL.free((Coin) entity);
            }
        }
    }

    /**
     * Метод вызывает у всех активных сущностей метод {@link Entity#update(float)}, проверят коллизии сущностей и т.д.
     * <br> Метод подвержен паузе.
     *
     * @param delta кол-во секунд прошедшее с последнего вызова метода.
     */
    private void update(float delta) {
        if (pause) return;

        // Проверка состояния волны.
        manageWave(delta);

        // ставим башни
        placeTower();

        // печатаем текст в диалог.
        if (speech.size > 0) {
            speechLabel.setText(speechLabel.getText().toString() + speech.removeFirst());
        }

        for (Entity entity : entities) {
            entity.update(delta);
        }

        // коллизии
        // из-за смещения индексов что-то может не обработаться, но в рамках этой игры это не страшно.
        for (int i = 0; i < entities.size; i++) {
            Entity a = entities.get(i);

            for (int j = 0; j < entities.size; j++) {
                Entity b = entities.get(j);

                if (a == b) continue;

                a.collide(b);
            }
        }

        animationStep += delta;
    }

    /**
     * Метод занимается запуском волн.
     *
     * @param delta кол-во секунд прошедшее с последнего вызова метода.
     */
    private void manageWave(float delta) {

        if (gameOver) return;

        // -2: сообщение о том что нужно потратить все деньги и переключится на -1.
        if (wave == -2) {
            wave++;
            say("Spend all your moneys on defense human, we will wait, maybe...");
            return;
        }

        // -1: ждем пока деньги не будут потрачены.
        if (wave == -1 && coinsNow >= TOWER_COST) {
            return;
        }

        // В случае если волна окончена:
        if (waveIsEnded) {

            Wave w = island.getWave(wave + 1);

            // Если волн больше нет, то заканчиваем игру.
            if (w == null) {

                gameOver = true;

                Ui.addPopUpWindow(stage, skin, "Victory!",
                        window -> window.add(
                                Ui.button("Play Again!", skin, (event, actor) -> game.reset())
                        ).padTop(20).grow());

                //noinspection SpellCheckingInspection
                say("Meooooooooooooooooooooooow!");

                return;
            }

            // Уменьшаем значение прогресса до нуля.
            if (progressBar.getValue() > 0) {
                progressBar.setValue(progressBar.getValue() - delta * 0.1f);
                return;
            }

            waveIsEnded = false;
            wave++;
            totalMobsInWave = 0;
            killsInWave = 0;
            spawnedMobsInWave = 0;

            for (int i = 0; i < entities.size; i++) {
                Entity entity = entities.get(i);
                if (entity instanceof Spawn) {
                    totalMobsInWave += w.setup((Spawn) entity);
                }
            }

            say("Attack! Wave number " + (wave + 1) + " will crash you!");
            return;
        }

        // Если все враги перебиты заканчиваем волну.
        if (totalMobsInWave == spawnedMobsInWave && killsInWave == totalMobsInWave) {
            waveIsEnded = true;
            say("Looks like you get lucky this time, actually it was act  " + (wave + 1) + " of " + island.getWavesAmount());
        }
    }

    /**
     * Метод выставляет башню при клике в нужно место, при условии, что хватает денег.
     */
    private void placeTower() {

        if (coinsNow < TOWER_COST) return;
        if (!Gdx.input.justTouched()) return;

        viewport.unproject(unProjectMouse.set(Gdx.input.getX(), Gdx.input.getY()));

        int ix = WorldCoords.fromScreenX(unProjectMouse.x);
        int iy = WorldCoords.fromScreenY(unProjectMouse.y);

        for (Entity entity : entities) {
            if (entity instanceof TowerSpot) {
                TowerSpot spot = (TowerSpot) entity;

                if (spot.isFree() && MathUtils.isEqual(spot.getX(), ix) && MathUtils.isEqual(spot.getY(), iy)) {

                    coinsNow -= TOWER_COST;

                    spot.setFree(false);
                    entities.add(new Tower(this, ix, iy, 3, 1, 2));
                    break;
                }
            }
        }
    }

    /**
     * Метод рисует игру и UI.
     *
     * @param delta кол-во секунд прошедшее с последнего вызова метода.
     */
    private void draw(float delta) {
        ScreenUtils.clear(Color.SKY);

        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);

        batch.begin();
        batch.setColor(Color.WHITE);

        // Нарисовать воду везде в видимости экрана с привязкой к сетке.

        int viewportMostLeftX = WorldCoords.fromScreenX(viewport.getCamera().position.x - viewport.getWorldWidth() / 2f) - 1;
        int viewportMostBottomY = WorldCoords.fromScreenY(viewport.getCamera().position.y - viewport.getWorldHeight() / 2f) - 1;

        int viewportMostRightX = WorldCoords.fromScreenX(viewport.getCamera().position.x + viewport.getWorldWidth() / 2f) + 1;
        int viewportMostTopY = WorldCoords.fromScreenY(viewport.getCamera().position.y + viewport.getWorldHeight() / 2f) + 1;

        for (int x = viewportMostLeftX; x < viewportMostRightX; x++) {
            for (int y = viewportMostBottomY; y < viewportMostTopY; y++) {
                batch.draw(waterAnimation.getKeyFrame(animationStep), WorldCoords.toScreenX(x), WorldCoords.toScreenY(y));
            }
        }

        //////////////////////////////////////////////////////////////

        for (int x = 0; x < island.getWidth(); x++) {
            for (int y = 0; y < island.getHeight(); y++) {

                IslandPoint point = island.getPoint(x, y);

                if (point.isGround()) {
                    batch.draw(groundTextures.get(point.getGroundStyle()), WorldCoords.toScreenX(x), WorldCoords.toScreenY(y));
                }

                switch (point.getPathType()) {
                    case ROAD:
                        batch.draw(pathTextures.get(point.getPathStyle()), WorldCoords.toScreenX(x), WorldCoords.toScreenY(y));
                        break;
                    case BRIDGE:
                        batch.draw(bridgeTextures.get(point.getPathStyle()), WorldCoords.toScreenX(x), WorldCoords.toScreenY(y));
                        break;
                }
            }
        }


        entities.sort(Entity.DRAW_ORDER_COMPARATOR);
        for (Entity entity : entities) {
            entity.draw(batch);
        }

        Array.ArrayIterator<ParticleEffectPool.PooledEffect> iterator = effects.iterator();

        while (iterator.hasNext()) {
            ParticleEffectPool.PooledEffect next = iterator.next();
            next.draw(batch, delta);
            if (next.isComplete()) {
                iterator.remove();
                next.free();
            }
        }

        batch.end();

        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            drawDebug();
        }

        stage.act(delta);
        stage.draw();
    }

    /**
     * Метод рисует элементы отладки для всех сущностей.
     */
    private void drawDebug() {
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        for (Entity entity : entities) {
            float v = entity.getDrawOrder();
            float x = WorldCoords.toScreenX(entity.getX());
            float y = WorldCoords.toScreenY(entity.getY());

            shapeRenderer.setColor(Color.GREEN);
            shapeRenderer.line(x - 10, v, x + 10, v);

            shapeRenderer.setColor(Color.BLACK);
            shapeRenderer.arc(x, y, 1, 0, 360);

            if (entity instanceof Mob) {
                Rectangle box = ((Mob) entity).updateAndGetBox();
                shapeRenderer.setColor(Color.PURPLE);
                shapeRenderer.rect(
                        WorldCoords.toScreenX(box.x), WorldCoords.toScreenY(box.y),
                        WorldCoords.toScreenX(box.width), WorldCoords.toScreenY(box.height)
                );
            }

            if (entity instanceof Tower) {
                Tower tower = (Tower) entity;
                shapeRenderer.setColor(Color.PURPLE);
                shapeRenderer.arc(WorldCoords.toScreenX(tower.getFirePointX()), WorldCoords.toScreenY(tower.getFirePointY()), 1, 0, 360);
            }
        }

        shapeRenderer.end();
    }

    /**
     * Метод добавляет текст в диалог заменяя предыдущий.
     *
     * @param text текст который нужно.
     */
    private void say(String text) {
        speechLabel.setText("");
        speech.clear();
        for (char c : text.toCharArray()) {
            speech.addLast(c);
        }
    }

    /**
     * Метод добавляет новую пулю в список активных сущностей.
     *
     * @param target цель в которую пуля была запущена.
     * @param tower  башня запустившая пулю.
     * @param damage урон наносимый пулей.
     */
    public void spawnBullet(Mob target, Tower tower, int damage) {
        Bullet value = BULLET_POOL.obtain();
        value.set(this, target, tower, damage);
        entities.add(value);
    }

    /**
     * Метод удаляет сущность из активных сущностей и дополнительно создает эффект из частичек.
     *
     * @param bullet сущность которую нужно удалить.
     */
    public void removeBullet(Bullet bullet) {
        if (entities.removeValue(bullet, true)) {

            ParticleEffectPool.PooledEffect obtain = cucumberExplosionPool.obtain();

            obtain.setPosition(WorldCoords.toScreenX(bullet.getX()), WorldCoords.toScreenY(bullet.getY()));
            obtain.scaleEffect(0.2f);
            effects.add(obtain);

            BULLET_POOL.free(bullet);

        }
    }

    /**
     * Метод добавляет нового врага в список активных сущностей.
     *
     * @param prototype прототип сущности, для внешнего вида, скорости, здоровья и т.д.
     * @param spawn     точка создавшая данную сущность.
     */
    public void spawnMob(MobPrototype prototype, Spawn spawn) {
        Mob value = MOB_POOL.obtain();
        value.set(this, prototype, spawn);
        entities.add(value);

        spawnedMobsInWave++;
        if (!gameOver) progressBar.setValue((float) spawnedMobsInWave / totalMobsInWave);
    }

    /**
     * Метод удаляет сущность из активных сущностей и дополнительно создает сущности монеток.
     *
     * @param mob сущность которую нужно удалить.
     */
    public void removeMob(Mob mob) {

        int coins = mob.getCoinsAmount();

        for (int i = 0; i < coins; i++) {
            spawnCoin(mob);
        }

        if (!gameOver) {
            killsInWave++;
        }

        if (entities.removeValue(mob, true)) {
            MOB_POOL.free(mob);
        }
    }

    /**
     * Метод добавляет новую монетку в список активных сущностей.
     *
     * @param mob цель с которой выпала монетка.
     */
    public void spawnCoin(Mob mob) {
        Coin coin = COIN_POOL.obtain();
        coin.set(this, mob);

        entities.add(coin);
    }

    /**
     * Метод удаляет сущность из активных сущностей.
     *
     * @param coin сущность которую нужно удалить.
     */
    public void removeCoin(Coin coin) {
        if (!gameOver) coinsNow++;

        if (entities.removeValue(coin, true)) {
            COIN_POOL.free(coin);
        }
    }

    /**
     * Завершить игру с сообщением о поражении.
     */
    public void gameOver() {

        if (gameOver) return;

        gameOver = true;

        Ui.addPopUpWindow(stage, skin, "Game Over", window -> {

            window.add(new Label("Wave: " + (wave + 1), skin));

            window.row();

            ProgressBar copy = new ProgressBar(progressBar.getMinValue(), progressBar.getMaxValue(), progressBar.getStepSize(), progressBar.isVertical(), skin);
            copy.setValue(progressBar.getValue());

            window.add(copy).grow();

            window.row();

            window.add(Ui.button("Retry", skin, (event, actor) -> game.reset())).padTop(20).grow();

        });
    }
}