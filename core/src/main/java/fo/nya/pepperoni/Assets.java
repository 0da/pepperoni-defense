package fo.nya.pepperoni;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.ArrayList;
import java.util.List;

/**
 * Небольшая 'инкапсуляция' имен и типов всех ресурсов, загружаемых в {@link AssetManager}.
 */
public final class Assets<X> {

    /**
     * Список всех существующих 'ресурсов', добавление происходит в конструкторе этого класса.
     */
    // Должен быть выше всех, чтоб при статической инициализации последующих объектов это поле уже было инициализировано.
    private static final List<Assets<?>> ALL = new ArrayList<>();

    ////////////////////////////////////////////////////////

    /**
     * Атлас со всеми текстурами присутствующим в игре.
     */
    public static final Assets<TextureAtlas> ATLAS = new Assets<>("PepperoniDefense.atlas", TextureAtlas.class);

    /**
     * Стиль UI.
     */
    public static final Assets<Skin> SKIN = new Assets<>("skin/PepperoniDefenseUi.json", Skin.class);

    /**
     * Небольшая 'частичка' происходящий при исчезновении огурца.
     */
    public static final Assets<ParticleEffect> CUCUMBER_EXPLOSION = new Assets<>("particles/cucumber-explosion.particle", ParticleEffect.class);


    ////////////////////////////////////////////////////////

    /**
     * Путь до загружаемого ресурса.
     */
    private final String path;

    /**
     * Тип загружаемого ресурса.
     */
    private final Class<X> type;

    /**
     * Создает новый 'ресурс'.
     *
     * @param path Путь до загружаемого ресурса.
     * @param type Тип загружаемого ресурса.
     * @implNote Конструктор добавляет созданный объект({@code this}) в список {@link #ALL};
     */
    private Assets(String path, Class<X> type) {
        this.path = path;
        this.type = type;

        ALL.add(this);
    }

    /**
     * Метод вызывает загрузку всех известных 'ресурсов' в предоставленный {@link AssetManager}.
     * <br>
     * <pre>{@code
     *      for (Assets<?> a : ALL) {
     *          assets.load(a.path, a.type);
     *      }
     * }
     * </pre>
     *
     * @param assets {@link AssetManager} в который нужно загрузить 'ресурсы'.
     */
    public static void loadAllInto(AssetManager assets) {
        for (Assets<?> a : ALL) {
            a.loadInto(assets);
        }
    }

    /**
     * Метод вызывает загрузку 'ресурса' в предоставленный {@link AssetManager}.
     * <br>
     * <pre>{@code
     *      assets.load(a.path, a.type);
     * }
     * </pre>
     *
     * @param assets {@link AssetManager} в который нужно загрузить 'ресурс'.
     */
    public void loadInto(AssetManager assets) {
        assets.load(path, type);
    }

    /**
     * Метод пытается получить 'ресурс' из предоставленного {@link AssetManager}.
     * <br>
     * <pre>{@code
     *      assets.get(path, type);
     * }
     * </pre>
     *
     * @param assets {@link AssetManager} из которого нужно получить 'ресурс'.
     */
    public X getFrom(AssetManager assets) {
        return assets.get(path, type);
    }

}
