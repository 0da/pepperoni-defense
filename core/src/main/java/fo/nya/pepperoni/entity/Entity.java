package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Comparator;

/**
 * Простой базовый интерфейс для всех сущностей.
 */
public interface Entity {

    /**
     * Компаратор для сортировки сущностей по результату метода {@link #getDrawOrder()}. Необходим для корректной отрисовки сущностей.
     */
    Comparator<Entity> DRAW_ORDER_COMPARATOR = Comparator.comparing(Entity::getDrawOrder).reversed();

    /**
     * Метод производит 'логические' операции над сущностями которые подвержены игровой паузе. Например, передвижение сущности.
     *
     * @param delta время в секундах прошедшее с прошлого вызова метода.
     */
    default void update(float delta) {}

    /**
     * Метод 'рисует' сущность на экране.
     *
     * @param batch {@link SpriteBatch} с помощью которого сущность будет нарисована.
     * @implNote Предполагается что метод {@link SpriteBatch#begin()} будет вызван до вызова этого метода, а {@link SpriteBatch#end()} будет после.
     * <pre>{@code
     *      SpriteBatch batch;
     *
     *      // ...
     *
     *      batch.begin();
     *      Entity.draw(batch);
     *      batch.end();
     * }
     * </pre>
     */
    void draw(SpriteBatch batch);

    /**
     * Метод должен возвращать приоритет сущности при рисовании.
     * Обычно это просто координата {@code Y}, т.к. чем выше сущность (в проекции 'top down')
     * тем раньше её необходимо нарисовать для достижения эффекта 'глубины'.
     *
     * @return приоритет сущности при рисовании.
     */
    float getDrawOrder();

    /**
     * Метод возвращает координату {@code x} в координатной системе 'мира'.
     *
     * @return координата {@code x} в координатной системе 'мира'.
     * @see fo.nya.pepperoni.util.WorldCoords
     */
    float getX();

    /**
     * Метод возвращает координату {@code y} в координатной системе 'мира'.
     *
     * @return координата {@code y} в координатной системе 'мира'.
     * @see fo.nya.pepperoni.util.WorldCoords
     */
    float getY();

    /**
     * Метод позволяет произвести проверку на 'коллизию' между двумя сущностями.
     *
     * @param entity другая сущность, с которой нужно произвести проверку.
     */
    // Простые коллизии каждый с каждым без каких либо деревьев, оптимизаций и т.д. так как это не требуется в масштабах игры.
    default void collide(Entity entity) {}

}
