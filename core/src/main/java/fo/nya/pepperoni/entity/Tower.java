package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.util.WorldCoords;

/**
 * Сущность башни.
 * <br> Башня ищет {@link Mob} в {@link #circle радиусе} своего действия, с наибольшим {@link Mob#getDistanceToPepperoni() приоритетом} и стреляет по этой цели раз в {@link #rate} секунд.
 */
public class Tower implements Entity {

    /**
     * Ссылка на объект 'игры', необходим для получения 'глобальных' значений и взаимодействия с другими сущностями.
     */
    private final Gameplay gameplay;

    /**
     * Текущий 'прогресс по зарядке пули'. Находится в диапазоне {@code [0, rate]}
     *
     * @see #rate
     */
    private float charge = 0;

    /**
     * Текущая цель башни.
     */
    private Mob target;

    /**
     * Позиция сущности и радиус в котором башня 'видит' врагов.
     */
    private final Circle circle = new Circle();

    /**
     * Урон наносимый {@link Bullet} выпущенными из этой башни.
     */
    private final int damage;

    /**
     * Скорость стрельбы, количество секунд на одну пулю.
     */
    private final float rate;

    /**
     * Отступ появления пули, нужно для того, чтоб визуально снаряд летел 'откуда надо'.
     */
    private final Vector2 firePointOffset = new Vector2(0.35f, 1.35f);

    /**
     * Случайное значение, прибавляемое к 'глобальному' таймеру анимации. Это нужно, чтоб анимация каждой сущности была чуть разной.
     */
    private final float animationOffset = MathUtils.random();

    /**
     * Создает новую башню.
     *
     * @param gameplay объект 'игры'.
     * @param x        позиция X башни в координатной системе 'мира'.
     * @param y        позиция Y башни в координатной системе 'мира'.
     * @param radius   радиус в котором башня 'видит' врагов.
     * @param damage   урон наносимый {@link Bullet} выпущенными из этой башни.
     * @param rate     скорость стрельбы, количество секунд на одну пулю.
     */
    public Tower(Gameplay gameplay, int x, int y, int radius, int damage, float rate) {
        this.gameplay = gameplay;
        this.circle.set(x, y, radius);
        this.damage = damage;
        this.rate = rate;
    }

    /**
     * @see Entity
     */
    @Override public void update(float delta) {
        charge += delta;
        if (charge >= rate) {

            if (target == null || !inRange(target.getX(), target.getY())) {
                target = null;
                charge = rate;
                return;
            }

            charge -= rate;

            gameplay.spawnBullet(target, this, damage);
        }
    }

    /**
     * @see Entity
     */
    @Override public float getDrawOrder() {
        return WorldCoords.toScreenY(getY()) + 20;
    }

    /**
     * @see Entity
     */
    @Override public float getX() {
        return circle.x;
    }

    /**
     * @see Entity
     */
    @Override public float getY() {
        return circle.y;
    }

    /**
     * @see Entity
     */
    @Override public void draw(SpriteBatch batch) {
        float x = WorldCoords.toScreenX(getX());
        float y = WorldCoords.toScreenY(getY()) + 4;

        batch.draw(gameplay.cucumberJarAnimation.getKeyFrame(animationOffset + gameplay.getAnimationStep()), x, y);

        batch.draw(gameplay.slingshotBackTexture, x, y + 15);

        if (charge >= rate) {
            batch.draw(gameplay.cucumberTexture, WorldCoords.toScreenX(getFirePointX()), WorldCoords.toScreenY(getFirePointY()));
        }

        batch.draw(gameplay.slingshotFrontTexture, x, y + 15);
    }

    /**
     * @see Entity
     */
    public void collide(Entity entity) {
        Mob mob;
        if (entity instanceof Mob) {
            mob = (Mob) entity;
        } else return;

        if (inRange(mob.getX(), mob.getY()) && (target == null || mob.getDistanceToPepperoni() < target.getDistanceToPepperoni())) {
            target = mob;
        }

    }

    /**
     * Метод проверят, содержится ли точка в радиусе охвата башни.
     *
     * @param x x точки для проверки.
     * @param y y точки для проверки
     * @return {@code true}, если точка {@code (x, y)} находится в {@link #circle}
     */
    public boolean inRange(float x, float y) {
        return circle.contains(x, y);
    }

    /**
     * Получить X точки в системе координат 'мира', из которой должна появляться пуля.
     *
     * @see #firePointOffset
     */
    public float getFirePointX() {return getX() + firePointOffset.x;}

    /**
     * Получить Y точки в системе координат 'мира', из которой должна появляться пуля.
     *
     * @see #firePointOffset
     */
    public float getFirePointY() {return getY() + firePointOffset.y;}

}
