package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.util.WorldCoords;

/**
 * Простая сущность для отображения декораций.
 */
public class Decoration implements Entity {

    /**
     * Позиция сущности.
     */
    private final Vector2 position = new Vector2();

    /**
     * Картинка декорации, выбирается случайно.
     */
    private final TextureRegion texture;

    /**
     * Создает новую декорацию со случайной картинкой.
     *
     * @param gameplay объект 'игры'.
     * @param x        позиция X декорации в координатной системе 'мира'.
     * @param y        позиция Y декорации в координатной системе 'мира'.
     * @param ground   {@code true} если декорация находится на земле, иначе на воде;
     */
    public Decoration(Gameplay gameplay, int x, int y, boolean ground) {
        this.position.set(x, y);

        if (ground) {
            texture = gameplay.groundDecorationTextures.random();
        } else {
            texture = gameplay.waterDecorationTextures.random();
        }
    }

    /**
     * @see Entity
     */
    @Override public void draw(SpriteBatch batch) {
        batch.draw(texture, WorldCoords.toScreenX(getX()), WorldCoords.toScreenY(getY()));
    }

    /**
     * @see Entity
     */
    @Override public float getDrawOrder() {
        return WorldCoords.toScreenY(getY());
    }

    /**
     * @see Entity
     */
    @Override public float getX() {
        return position.x;
    }

    /**
     * @see Entity
     */
    @Override public float getY() {
        return position.y;
    }
}
