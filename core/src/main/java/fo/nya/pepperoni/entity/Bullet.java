package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.util.WorldCoords;
import lombok.Getter;

/**
 * Сущность 'пули'.
 * <br>
 * <br> Сущность которая перемещается из {@link #position изначальной позиции}, в {@link #direction определенном направлении}, с {@link #speed указанной скоростью}.
 * В случае если пуля за {@link #lifetime время своей жизни} касается цели, то она наносит {@link #damage определенный урон} и пропадает.
 */
public class Bullet implements Entity {

    /**
     * Ссылка на объект 'игры', необходим для получения 'глобальных' значений и взаимодействия с другими сущностями.
     */
    private Gameplay gameplay;

    /**
     * Урон наносимый это сущностью в случае попадания по цели.
     */
    @Getter private int damage;

    /**
     * Скорость перемещения сущности.
     */
    private float speed;

    /**
     * Время 'жизни' сущности в секундах, если время закончится, то сущность пропадет даже если не попала по цели.
     */
    private float lifetime;

    /**
     * Текущее направление полета сущности.
     */
    private final Vector2 direction = new Vector2();

    /**
     * Текущая позиция сущности.
     */
    private final Vector2 position = new Vector2();

    /**
     * Случайное значение, прибавляемое к 'глобальному' таймеру анимации. Это нужно, чтоб анимация каждой сущности была чуть разной.
     */
    private final float animationOffset = MathUtils.random();

    /**
     * @see Entity
     */
    @Override public void update(float delta) {
        lifetime -= delta;

        if (lifetime < 0) {
            gameplay.removeBullet(this);
            return;
        }

        position.add(direction.x * delta * speed, direction.y * delta * speed);
    }

    /**
     * @see Entity
     */
    @Override public float getDrawOrder() {
        return WorldCoords.toScreenY(getY());
    }

    /**
     * @see Entity
     */
    @Override public void draw(SpriteBatch batch) {
        TextureRegion cc = gameplay.cucumberTexture;

        int width = cc.getRegionWidth();
        float halfWidth = width * 0.5f;

        int height = cc.getRegionHeight();
        float halfHeight = width * 0.5f;

        batch.draw(cc, WorldCoords.toScreenX(position.x) - halfWidth, WorldCoords.toScreenY(position.y) - halfHeight, halfWidth, halfHeight, width, height,
                1, 1, (gameplay.getAnimationStep() + animationOffset) * 360, true);
    }

    /**
     * @see Entity
     */
    @Override public float getX() {
        return position.x;
    }

    /**
     * @see Entity
     */
    @Override public float getY() {
        return position.y;
    }

    /**
     * Метод задает необходимые параметры для функционирования объекта.
     *
     * @param gameplay ссылка на объект 'игры'.
     * @param target   цель в которую пуля была запущена.
     * @param tower    башня запустившая пулю.
     * @param damage   урон наносимый пулей.
     */
    public void set(Gameplay gameplay, Mob target, Tower tower, int damage) {
        this.gameplay = gameplay;
        this.damage = damage;
        this.lifetime = 1f;
        this.speed = 4f;

        this.position.set(tower.getFirePointX(), tower.getFirePointY());
        Rectangle box = target.updateAndGetBox();

        this.direction.set(box.x, box.y)
                .add(box.width * 0.5f, box.height * 0.5f)
                .sub(position)
                .nor();
    }
}
