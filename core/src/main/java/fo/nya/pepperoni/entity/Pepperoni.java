package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.util.WorldCoords;

/**
 * Сущность 'пепперони'.
 * <br>
 * <br> Сущность которая сама по себе ничего не делает, но в случае если {@link Mob} 'наступит' на клетку с этой сущностью, то здоровье пепперони уменьшается на один.
 * Когда здоровье достигает нуля, вызывается метод {@link Gameplay#gameOver()}.
 */
public class Pepperoni implements Entity {

    /**
     * Ссылка на объект 'игры', необходим для получения 'глобальных' значений и взаимодействия с другими сущностями.
     */
    private final Gameplay gameplay;

    /**
     * Текущее здоровье сущности.
     */
    private int health = 20;

    /**
     * Позиция сущности.
     */
    private final Vector2 position = new Vector2();

    /**
     * Создает новую пепперони с 20 очками здоровья.
     *
     * @param gameplay объект 'игры'.
     * @param x        позиция X пепперони в координатной системе 'мира'.
     * @param y        позиция Y пепперони в координатной системе 'мира'.
     */
    public Pepperoni(Gameplay gameplay, int x, int y) {
        this.gameplay = gameplay;
        this.position.set(x, y);
    }

    /**
     * @see Entity
     */
    @Override public float getDrawOrder() {
        return WorldCoords.toScreenY(getY()) + 8;
    }

    /**
     * @see Entity
     */
    @Override public void draw(SpriteBatch batch) {
        batch.draw(gameplay.pepperoniTextures.get(Math.max(health, 0)), WorldCoords.toScreenX(getX()) - 16, WorldCoords.toScreenY(getY()));
    }

    /**
     * @see Entity
     */
    @Override public float getX() {
        return position.x;
    }

    /**
     * @see Entity
     */
    @Override public float getY() {
        return position.y;
    }

    /**
     * Уменьшить здоровье на один пункт. В случае если здоровье достигнет нуля, то будет вызван метод {@link Gameplay#gameOver()}.
     */
    public void decrementHealth() {
        health--;

        if (health <= 0) {
            gameplay.gameOver();
        }
    }
}
