package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Queue;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.MobPrototype;
import fo.nya.pepperoni.util.WorldCoords;
import lombok.Getter;

/**
 * Сущность точки из которой появляются {@link Mob}.
 */
public class Spawn implements Entity {

    /**
     * Ссылка на объект 'игры', необходим для получения 'глобальных' значений и взаимодействия с другими сущностями.
     */
    private final Gameplay gameplay;

    /**
     * ID этой сущности, задается из изначальной карты.
     * Необходим для того, чтоб настраивать очередь врагов, и скорость воспроизведения индивидуально для каждой точки.
     *
     * <br> Находится в диапазоне {@code [0, 9]}
     */
    @Getter private final int id;

    /**
     * Позиция сущности.
     */
    private final Vector2 position = new Vector2();

    /**
     * Скорость воспроизведения врагов.
     * <br><i>{@code 0} - мгновенное воспроизведение, 1 - одна секунда и т.д.</i>
     */
    private float rate;

    /**
     * Текущий 'прогресс по воспроизведению' врага. Находится в диапазоне {@code [0, rate]}
     *
     * @see #rate
     */
    private float charge;

    /**
     * Очередь врагов для воспроизведения.
     */
    private final Queue<MobPrototype> queue = new Queue<>();

    /**
     * Создает новую точку с пустой очередью и нулевой скоростью воспроизведения.
     *
     * @param gameplay объект 'игры'.
     * @param id       ID этой сущности.
     * @param x        позиция X точки появления в координатной системе 'мира'.
     * @param y        позиция Y точки появления в координатной системе 'мира'.
     */
    public Spawn(Gameplay gameplay, int id, int x, int y) {
        this.gameplay = gameplay;
        this.id = id;
        this.position.set(x, y);
    }

    /**
     * @see Entity
     */
    @Override public void update(float delta) {
        charge += delta;

        if (charge >= rate) {
            charge = 0;

            if (queue.notEmpty()) {
                MobPrototype prototype = queue.removeFirst();
                gameplay.spawnMob(prototype, this);
            }
            /*
            // если надо бесконечную случайность:
            else {
                gameplay.spawnMob(MobPrototype.random(), x, y);
            }
            */
        }
    }

    /**
     * @see Entity
     */
    @Override public float getDrawOrder() {
        return WorldCoords.toScreenY(getY()) + 16;
    }

    /**
     * @see Entity
     */
    @Override public float getX() {
        return position.x;
    }

    /**
     * @see Entity
     */
    @Override public float getY() {
        return position.y;
    }

    /**
     * @see Entity
     */
    @Override public void draw(SpriteBatch batch) {
        batch.draw(gameplay.catSpawnTexture, WorldCoords.toScreenX(getX()) - 16, WorldCoords.toScreenY(getY()));
    }

    /**
     * Метод 'настраивает' данную сущность.
     *
     * @param rate       скорость воспроизведения врагов.
     * @param prototypes враги которые должны появится из этой точки, коллекция не модифицируется.
     */
    public void setup(float rate, Array<MobPrototype> prototypes) {
        this.rate = rate;
        for (int i = 0; i < prototypes.size; i++) {
            this.queue.addLast(prototypes.get(i));
        }
    }
}
