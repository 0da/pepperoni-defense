package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.util.WorldCoords;

/**
 * Сущность 'монетки'.
 * <br>
 * <br> Сущность 'выпадающая' с врагов после их смерти на ту же самую {@link #position позицию}.
 * Какое-то время движется в {@link #velocity случайном направлении}. По истечению {@link #lifetime срока жизни} начисляется на баланс игроку и пропадает с экрана.
 */
public class Coin implements Entity {

    /**
     * Ссылка на объект 'игры', необходим для получения 'глобальных' значений и взаимодействия с другими сущностями.
     */
    private Gameplay gameplay;

    /**
     * Время 'жизни' сущности в секундах, если время закончится, то сущность пропадет и начисляется игроку на баланс.
     */
    private float lifetime;

    /**
     * Текущая позиция сущности.
     */
    private final Vector2 position = new Vector2();

    /**
     * Текущей вектор движения сущности, со временем уменьшается до нуля.
     */
    private final Vector2 velocity = new Vector2();

    /**
     * Случайное значение, прибавляемое к 'глобальному' таймеру анимации. Это нужно, чтоб анимация каждой сущности была чуть разной.
     */
    private final float animationOffset = MathUtils.random();

    /**
     * @see Entity
     */
    @Override public void update(float delta) {

        lifetime -= delta;

        if (lifetime < 0) {
            gameplay.removeCoin(this);
            return;
        }

        velocity.sub(velocity.x * 0.9f * delta, velocity.y * 0.9f * delta);

        position.add(velocity.x * delta, velocity.y * delta);
    }

    /**
     * @see Entity
     */
    @Override public float getDrawOrder() {
        return WorldCoords.toScreenY(getY());
    }

    /**
     * @see Entity
     */
    @Override public void draw(SpriteBatch batch) {
        if (lifetime < 1) {
            if (((int) (lifetime * 20)) % 2 == 0) return;
        }
        float stateTime = animationOffset + gameplay.getAnimationStep();
        batch.draw(gameplay.coinAnimation.getKeyFrame(stateTime), WorldCoords.toScreenX(getX()), WorldCoords.toScreenY(getY()));
    }

    /**
     * @see Entity
     */
    @Override public float getX() {
        return position.x;
    }

    /**
     * @see Entity
     */
    @Override public float getY() {
        return position.y;
    }

    /**
     * Метод задает необходимые параметры для функционирования объекта.
     *
     * @param gameplay ссылка на объект 'игры'.
     * @param mob      цель с которой выпала монетка.
     */
    public void set(Gameplay gameplay, Mob mob) {

        this.gameplay = gameplay;

        this.position.set(mob.getX(), mob.getY());
        this.velocity.set(MathUtils.random(-1f, 1f), MathUtils.random(-1f, 1f)).nor();

        this.lifetime = MathUtils.random(3, 5);
    }

}
