package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.MobPrototype;
import fo.nya.pepperoni.island.IslandPoint;
import fo.nya.pepperoni.util.WorldCoords;
import lombok.Getter;

/**
 * Сущность 'врага'.
 * <br>
 * <br> Сущность перемещающая от {@link #currentMapPosition} к {@link #targetMapPosition} с определенной {@link MobPrototype#speed скоростью}.
 * Когда сущность достигает цели, происходит примерно следующее:
 * <pre>{@code
 *      currentMapPosition = targetMapPosition;
 *      targetMapPosition = currentMapPosition.next();
 * }</pre>
 * <br> В случае если сущность достигает {@link Pepperoni}, то по колбасе наносится урон, но не более одного раза, за один вход на клетку.
 * <br> В случае если сущность умирает, то она создает {@link Coin монетки} в количестве {@link #getCoinsAmount()}.
 */
public class Mob implements Entity, Pool.Poolable {

    /**
     * Ссылка на объект 'игры', необходим для получения 'глобальных' значений и взаимодействия с другими сущностями.
     */
    private Gameplay gameplay;

    /**
     * Текущий 'прототип' врага, отвечает за внешний вид, скорость, здоровье и т.д.
     */
    @Getter private MobPrototype prototype;

    /**
     * Текущий прогресс в движении между {@link #currentMapPosition} и {@link Mob#targetMapPosition}.
     */
    private float progress;

    /**
     * Количество клеток до {@link Pepperoni}, если считать от {@link #targetMapPosition}.
     * <br> В случае если в поле зрения {@link Tower башни} несколько сущностей, то башня будет стрелять по той что ближе к пепперони.
     */
    @Getter private int distanceToPepperoni;

    /**
     * Текущее здоровье сущности.
     */
    private int health;

    /**
     * Флаг того что был нанесен урона по {@link Pepperoni}.
     * Этот флаг сбрасывается каждый раз когда сущность достигает {@link #targetMapPosition}
     * и устанавливается в {@code true} если пепперони лежит на {@link #currentMapPosition}
     */
    private boolean pepperoniBitten;

    /**
     * Координаты клетки на карте которую сущность посетила последний раз.
     */
    public final Vector2 currentMapPosition = new Vector2();

    /**
     * Координаты клетки на карте к которой сущность движется в текущий момент.
     */
    public final Vector2 targetMapPosition = new Vector2();

    /**
     * Текущая реальная позиция сущности. Находится между {@link #currentMapPosition} и {@link Mob#targetMapPosition}.
     */
    private final Vector2 position = new Vector2();

    /**
     * 'Collision Box' сущности, со всеми необходимыми отступами. Обновляется каждый раз при обращении к методу {@link #updateAndGetBox()}.
     */
    private final Rectangle box = new Rectangle();

    /**
     * Случайное значение, прибавляемое к 'глобальному' таймеру анимации. Это нужно, чтоб анимация каждой сущности была чуть разной.
     */
    private final float animationOffset = MathUtils.random();

    /**
     * @see Entity
     */
    @Override public void update(float delta) {
        progress += delta * prototype.speed;

        if (progress >= 1) {
            progress -= 1;

            currentMapPosition.set(targetMapPosition);

            int currentX = MathUtils.round(currentMapPosition.x);
            int currentY = MathUtils.round(currentMapPosition.y);

            IslandPoint target = gameplay.getIsland().navigateToPepperoni(currentX, currentY);
            targetMapPosition.set(target.getX(), target.getY());
            distanceToPepperoni = target.getDistanceToPepperoni();

            if (!gameplay.isGameOver()) {
                pepperoniBitten = false;
            }
        }

        position.x = Interpolation.linear.apply(currentMapPosition.x, targetMapPosition.x, progress);
        position.y = Interpolation.linear.apply(currentMapPosition.y, targetMapPosition.y, progress);

    }

    /**
     * @see Entity
     */
    @Override public float getDrawOrder() {
        return WorldCoords.toScreenY(position.y) + 4;
    }

    /**
     * @see Entity
     */
    @Override public void draw(SpriteBatch batch) {
        String direction;

        if (currentMapPosition.x == targetMapPosition.x) {
            direction = (currentMapPosition.y < targetMapPosition.y ? prototype.animationNameUp : prototype.animationNameDown);
        } else {
            direction = (currentMapPosition.x < targetMapPosition.x ? prototype.animationNameRight : prototype.animationNameLeft);
        }


        TextureRegion frame = gameplay.catWalkAnimation.get(direction)
                .getKeyFrame(gameplay.getAnimationStep() + animationOffset);

        float x = WorldCoords.toScreenX(position.x);
        float y = WorldCoords.toScreenY(position.y);

        float space = 16f / (health + 1);

        float heartHalfWidth = 4f;

        for (int i = 0; i < health; i++) {
            batch.draw(gameplay.heartTexture, x - 1 + space * (i + 1) - heartHalfWidth, y + 21);
        }

        batch.draw(frame, x, y + 4);
    }

    /**
     * @see Entity
     */
    @Override public float getX() {
        return position.x;
    }

    /**
     * @see Entity
     */
    @Override public float getY() {
        return position.y;
    }

    /**
     * @see Entity
     */
    @Override public void collide(Entity entity) {

        if (entity instanceof Bullet) {
            Bullet bullet = (Bullet) entity;

            if (updateAndGetBox().contains(bullet.getX(), bullet.getY())) {
                gameplay.removeBullet(bullet);

                health -= bullet.getDamage();

                if (health <= 0) {
                    gameplay.removeMob(this);
                }
            }

        } else if (!pepperoniBitten && entity instanceof Pepperoni) {
            Pepperoni pepperoni = (Pepperoni) entity;

            if (MathUtils.isEqual(pepperoni.getX(), currentMapPosition.x) && MathUtils.isEqual(pepperoni.getY(), currentMapPosition.y)) {
                pepperoni.decrementHealth();
                pepperoniBitten = true;
            }
        }
    }

    // Выставляем позицию за пределами карты, чтоб башни не стреляли по призракам.
    @Override public void reset() {
        position.set(-100, -100);
        currentMapPosition.set(-100, -100);
        targetMapPosition.set(-100, -100);
    }

    /**
     * Метод обновляет и возвращает 'Collision Box' сущности, со всеми необходимыми отступами.
     *
     * @return 'Collision Box' сущности, со всеми необходимыми отступами.
     */
    public Rectangle updateAndGetBox() {
        return box.set(getX() + 1 / 16f, getY() + 4 / 16f, 12 / 16f, 14 / 16f);
    }

    /**
     * Количество монеток которое выпадает в этой сущности.
     */
    public int getCoinsAmount() {
        return prototype.health;
    }

    /**
     * Метод задает необходимые параметры для функционирования объекта.
     *
     * @param gameplay  ссылка на объект 'игры'.
     * @param prototype прототип сущности, для внешнего вида, скорости, здоровья и т.д.
     * @param spawn     точка создавшая данную сущность.
     */
    public void set(Gameplay gameplay, MobPrototype prototype, Spawn spawn) {
        this.gameplay = gameplay;
        this.prototype = prototype;
        this.health = prototype.health;

        float x = spawn.getX();
        float y = spawn.getY();

        this.currentMapPosition.set(x, y);

        IslandPoint point = gameplay.getIsland().navigateToPepperoni(MathUtils.round(x), Math.round(y));
        this.targetMapPosition.set(point.getX(), point.getY());
        this.distanceToPepperoni = point.getDistanceToPepperoni();

        this.progress = 0;
        this.pepperoniBitten = false;
    }


}
