package fo.nya.pepperoni.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.util.WorldCoords;
import lombok.Getter;
import lombok.Setter;

/**
 * Сущность на которую нужно ставить {@link Tower башню}. Одна такая сущность - одно место для башни.
 */
public class TowerSpot implements Entity {

    /**
     * Ссылка на объект 'игры', необходим для получения 'глобальных' значений и взаимодействия с другими сущностями.
     */
    private final Gameplay gameplay;

    /**
     * Позиция сущности.
     */
    private final Vector2 position = new Vector2();

    /**
     * Флаг того, что данное место еще свободно.
     */
    @Getter @Setter private boolean free = true;

    /**
     * Создает новое свободное место для башни.
     *
     * @param gameplay объект 'игры'.
     * @param x        позиция X места для башни в координатной системе 'мира'.
     * @param y        позиция Y места для башни в координатной системе 'мира'.
     */
    public TowerSpot(Gameplay gameplay, int x, int y) {
        this.gameplay = gameplay;
        this.position.set(x, y);
    }

    /**
     * @see Entity
     */
    @Override public float getDrawOrder() {
        return Integer.MAX_VALUE;
    }

    /**
     * @see Entity
     */
    @Override public float getX() {
        return position.x;
    }

    /**
     * @see Entity
     */
    @Override public float getY() {
        return position.y;
    }

    /**
     * @see Entity
     */
    @Override public void draw(SpriteBatch batch) {
        batch.draw(gameplay.towerSpotTexture, WorldCoords.toScreenX(getX()), WorldCoords.toScreenY(getY()));
    }
}
