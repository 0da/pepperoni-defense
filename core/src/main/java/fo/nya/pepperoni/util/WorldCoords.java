package fo.nya.pepperoni.util;

/**
 * Класс-утилита для перевода координат из координатной системы 'мира' в координатную систему 'экрана'.
 */
public final class WorldCoords {

    /**
     * Ширина одной клетки в координатной системе 'экрана'.
     */
    public static final int TILE_WIDTH = 16;

    /**
     * Высота одной клетки в координатной системе 'экрана'.
     */
    public static final int TILE_HEIGHT = 16;

    /**
     * Метод переводит координату {@code x} из координатной системы 'мира' в координатную систему 'экрана'
     *
     * @param x координата {@code x} в координатной системе 'мира'.
     * @return координата {@code x} в координатной системе 'экрана'.
     */
    public static float toScreenX(float x) {
        return x * TILE_WIDTH;
    }

    /**
     * Метод переводит координату {@code y} из координатной системы 'мира' в координатную систему 'экрана'
     *
     * @param y координата {@code y} в координатной системе 'мира'.
     * @return координата {@code y} в координатной системе 'экрана'.
     */
    public static float toScreenY(float y) {
        return y * TILE_HEIGHT;
    }

    /**
     * Метод переводит координату {@code x} из координатной системы 'экрана' в координатную систему 'мира'
     *
     * @param x координата {@code x} в координатной системе 'экрана'.
     * @return координата {@code x} в координатной системе 'мира'.
     */
    public static int fromScreenX(float x) {
        return (int) (x / TILE_WIDTH);
    }

    /**
     * Метод переводит координату {@code y} из координатной системы 'экрана' в координатную систему 'мира'
     *
     * @param y координата {@code y} в координатной системе 'экрана'.
     * @return координата {@code y} в координатной системе 'мира'.
     */
    public static int fromScreenY(float y) {
        return (int) (y / TILE_HEIGHT);
    }
}
