package fo.nya.pepperoni.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;

/**
 * Класс для отрисовки спрайтовых анимаций в UI.
 */
public class AnimatedDrawable extends BaseDrawable {

    /**
     * Анимация для отображения.
     */
    private final Animation<TextureRegion> animation;

    /**
     * Счетчик времени для отображения анимации.
     */
    private float step;

    public AnimatedDrawable(Animation<TextureRegion> animation) {
        this.animation = animation;

        TextureRegion example = animation.getKeyFrame(0);

        setMinWidth(example.getRegionWidth());
        setMinHeight(example.getRegionHeight());
    }

    @Override public void draw(Batch batch, float x, float y, float width, float height) {

        step += Gdx.graphics.getDeltaTime();

        TextureRegion frame = animation.getKeyFrame(step);

        batch.draw(frame, x, y, width, height);
    }
}
