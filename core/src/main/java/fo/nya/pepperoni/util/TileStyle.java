package fo.nya.pepperoni.util;

/**
 * Класс-утилита для определения 'стиля' клеток в зависимости от соседних клеток такого же типа.
 *
 * @see #style4(boolean, boolean, boolean, boolean)
 * @see #style8(boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean)
 */
public final class TileStyle {

    /**
     * Вычислить 'стиль' клетки в зависимости от четырех соседних клеток: верхней, нижней, левой, правой.
     *
     * @param up    присутствие верхней клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param down  присутствие нижней клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param left  присутствие левой клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param right присутствие правой клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @return Стиль клетки в формате числа, в диапазоне {@code [0, 15]} в зависимости от 'соседних' клеток.
     * @see #style8(boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean)
     */
    public static int style4(boolean up, boolean down, boolean left, boolean right) {
        /*     8
         *   2 # 4
         *     1
         */

        int mask = 0;
        if (up) mask |= 8;
        if (left) mask |= 2;
        if (down) mask |= 1;
        if (right) mask |= 4;
        return mask;
    }

    /**
     * Вычислить 'стиль' клетки в зависимости от восьми соседних клеток: верхней, нижней, левой, правой, верхней левой, верхней правой, нижней левой, нижней правой.
     *
     * @param up        присутствие верхней клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param down      присутствие нижней клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param left      присутствие левой клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param right     присутствие правой клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param upLeft    присутствие верхней левой клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param upRight   присутствие верхней правой клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param downLeft  присутствие нижней левой клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @param downRight присутствие нижней правой клетки, {@code true} если эта клетка 'такая же', и нужно учитывать её в вычислении стиля.
     * @return Стиль клетки в формате числа, в диапазоне {@code [0, 46]} в зависимости от 'соседних' клеток.
     * @see #style4(boolean, boolean, boolean, boolean)
     */
    public static int style8(boolean up, boolean down, boolean left, boolean right, boolean upLeft, boolean upRight, boolean downLeft, boolean downRight) {
        /*   32  64  128
         *    8   #   16
         *    1   2    4
         */

        int mask = 0;
        if (up) mask |= 64;
        if (left) mask |= 8;
        if (down) mask |= 2;
        if (right) mask |= 16;


        if (up && left && upLeft) mask |= 32;
        if (down && left && downLeft) mask |= 1;
        if (down && right && downRight) mask |= 4;
        if (up && right && upRight) mask |= 128;

        return pack(mask);
    }

    /**
     * Метод пакует 'сырой' результат метода {@link #style8(boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean)} в диапазон {@code [0, 46]}.
     *
     * @param style 'сырой' стиль.
     * @return Число в диапазоне {@code [0, 46]}.
     */
    private static int pack(int style) {
        switch (style) {
            case 2:
                return 0;
            case 8:
                return 1;
            case 10:
                return 2;
            case 11:
                return 3;
            case 16:
                return 4;
            case 18:
                return 5;
            case 22:
                return 6;
            case 24:
                return 7;
            case 26:
                return 8;
            case 27:
                return 9;
            case 30:
                return 10;
            case 31:
                return 11;
            case 64:
                return 12;
            case 66:
                return 13;
            case 72:
                return 14;
            case 74:
                return 15;
            case 75:
                return 16;
            case 80:
                return 17;
            case 82:
                return 18;
            case 86:
                return 19;
            case 88:
                return 20;
            case 90:
                return 21;
            case 91:
                return 22;
            case 94:
                return 23;
            case 95:
                return 24;
            case 104:
                return 25;
            case 106:
                return 26;
            case 107:
                return 27;
            case 120:
                return 28;
            case 122:
                return 29;
            case 123:
                return 30;
            case 126:
                return 31;
            case 127:
                return 32;
            case 151:
                return 6;
            case 208:
                return 33;
            case 210:
                return 34;
            case 214:
                return 35;
            case 216:
                return 36;
            case 218:
                return 37;
            case 219:
                return 38;
            case 222:
                return 39;
            case 223:
                return 40;
            case 248:
                return 41;
            case 250:
                return 42;
            case 251:
                return 43;
            case 254:
                return 44;
            case 255:
                return 45;
            case 0:
                return 46;
            default:
                throw new IllegalStateException("Style " + style + " is impossible.");
        }
    }
}



