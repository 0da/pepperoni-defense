package fo.nya.pepperoni.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Класс-утилита чуть упрощающий создание UI.
 */
public final class Ui {

    /**
     * Метод создает {@link TextButton} с заданным текстом и обработчиком 'клика' по кнопке.
     *
     * @param text     текст, который будет отображаться на кнопке.
     * @param skin     стиль кнопки.
     * @param listener обработчик 'клика'.
     * @return Новая кнопка с заданным текстом и обработчиком 'клика'.
     */
    public static TextButton button(String text, Skin skin, BiConsumer<Event, Actor> listener) {
        TextButton button = new TextButton(text, skin);

        button.addListener(new ChangeListener() {
            @Override public void changed(ChangeEvent event, Actor actor) {
                listener.accept(event, actor);
            }
        });

        return button;
    }

    /**
     * Метод создает новое UI окно с заданными заголовком.
     * <br> Для того чтоб окно было по центру экрана дополнительно создается объект {@link Table}.
     * Поэтому для удаления окна нужно использовать следующий код:
     * <pre>{@code
     * window.getParent().remove()
     * }</pre>
     *
     * @param stage 'сцена' куда будет добавлено новое окно.
     * @param skin  стиль окна.
     * @param title заголовок окна.
     * @param setup действие для дополнительной настройки окна вызываемое после того как окно было создано и настроен.
     * @implNote Окно создается со стилем {@code 'shadowed'}
     */
    public static void addPopUpWindow(Stage stage, Skin skin, String title, Consumer<Window> setup) {
        Table table = new Table();
        table.setFillParent(true);
        table.setTouchable(Touchable.enabled);

        Window window = new Window(title, skin, "shadowed");
        window.setMovable(false);

        window.padTop(50);
        window.getTitleLabel().setAlignment(Align.center);

        table.add(window).width(Value.percentWidth(0.25f, table));

        stage.addActor(table);

        window.setColor(1, 1, 1, 0);
        window.addAction(Actions.fadeIn(0.2f));

        setup.accept(window);

    }
}
