package fo.nya.pepperoni.island;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Класс представляющий собой одну точку карты.
 */
@RequiredArgsConstructor public class IslandPoint {

    /**
     * Координата X на карте.
     */
    @Getter final int x;

    /**
     * Координата Y на карте.
     */
    @Getter final int y;

    /**
     * Символ, которым была обозначена эта точка.
     */
    final char symbol;

    /**
     * Дистанция до ближайшей пепперони, актуально только для дорог.
     */
    @Getter int distanceToPepperoni;

    /**
     * {@code true} если эта точка является землей, иначе вода.
     */
    @Getter boolean ground;

    /**
     * Тип дороги.
     */
    @Getter IslandPathType pathType = IslandPathType.NONE;

    /**
     * Стиль дороги.
     *
     * @see fo.nya.pepperoni.util.TileStyle
     */
    @Getter int pathStyle = 0;

    /**
     * Стиль земли.
     *
     * @see fo.nya.pepperoni.util.TileStyle
     */
    @Getter int groundStyle = 0;
}
