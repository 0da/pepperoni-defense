package fo.nya.pepperoni.island;


import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Queue;
import fo.nya.pepperoni.Gameplay;
import fo.nya.pepperoni.MobPrototype;
import fo.nya.pepperoni.Wave;
import fo.nya.pepperoni.entity.Decoration;
import fo.nya.pepperoni.entity.Pepperoni;
import fo.nya.pepperoni.entity.Spawn;
import fo.nya.pepperoni.entity.TowerSpot;
import fo.nya.pepperoni.util.TileStyle;
import lombok.Getter;

/**
 * Карта уровня, содержит данные о ландшафте, пути, и игровых сущностях таких как пепперони.
 */
public class Island {

    /*
     * Волны настраиваются в формате:
     *
     * <начало строки>!<int номер волны>,<int id точки появления>,<float rate>,<id врага от 0 до 2>,<id врага от 0 до 2>,...
     *
     * -----
     * Точки появления обозначаются цифрами.
     *
     * -----
     *
     * Одна строчка в тестовом виде равно одной строчке на экране.
     *
     */

    /**
     * Символ, которым обозначается пепперони.
     */
    private final static char PEPPERONI_SYMBOL = 'P';

    /**
     * Символ, которым обозначается место для башни.
     */
    private final static char TOWER_SYMBOL = 'T';

    /**
     * Символ, которым обозначается вода.
     */
    private final static char WATER_SYMBOL = '~';

    /**
     * Символ, которым обозначается мост над водой.
     */
    private final static char WATER_BRIDGE_SYMBOL = '+';

    /**
     * Символ, которым обозначается мост над землей.
     */
    private final static char GROUND_BRIDGE_SYMBOL = '=';

    /**
     * Символ, которым обозначается дорога по земле.
     */
    private final static char PATH_SYMBOL = '#';

    /**
     * Символ, которым обозначается декорация на земле.
     */
    private final static char GROUND_DECORATION_SYMBOL = 'd';

    /**
     * Символ, которым обозначается декорация на воде.
     */
    private final static char WATER_DECORATION_SYMBOL = 'D';

    /**
     * Настройки волн.
     */
    private final Array<Wave> waves = new Array<>();

    /**
     * Игровое поле.
     */
    private final Array</* x */Array</* y */IslandPoint>> tiles = new Array<>();

    /**
     * Ширина карты. Карта может быть не прямоугольной, ширина зависит от максимально X.
     */
    @Getter private int width;

    /**
     * Высота карты. Карта может быть не прямоугольной, высота зависит от максимально Y.
     */
    @Getter private int height;

    /**
     * Создать новую карту на основе текстового представления.
     *
     * @param map текстовое представление карты.
     */
    public Island(String map) {

        Queue<IslandPoint> points = parse(map);

        pathfinding(points);

        calculateStyle();
    }

    /**
     * Метод занимается трансформацией текстового представления в карту.
     *
     * @param text текстовое представление карты.
     * @return коллекция с точками на которых лежит пепперони.
     */
    private Queue<IslandPoint> parse(String text) {

        String[] split = text.split("\n");

        // Инвертируем массив, так как рисовка тоже инвертирована... ¯\_(ツ)_/¯
        for (int i = 0; i < split.length / 2; i++) {
            int opposite = split.length - 1 - i;

            String s = split[i];
            split[i] = split[opposite];
            split[opposite] = s;
        }

        Queue<IslandPoint> pepperonis = new Queue<>();

        int skipped = 0;

        for (int y = 0; y < split.length; y++) {

            String s = split[y];

            // настройки волн
            if (s.length() > 0 && s.charAt(0) == '!') {

                skipped++;

                String[] numbers = s.substring(1).split(",");

                try {

                    int waveId = Integer.parseInt(numbers[0]);

                    // берем волну, если такой нет, то вставляем её.
                    Wave wave = getWave(waveId);

                    if (wave == null) {
                        while (waves.size <= waveId) {
                            waves.add(new Wave());
                        }

                        wave = waves.get(waveId);
                    }

                    int spawnId = Integer.parseInt(numbers[1]);

                    wave.setRate(spawnId, Float.parseFloat(numbers[2]));


                    Array<MobPrototype> prototypes = new Array<>(numbers.length - 3);
                    for (int i = 3; i < numbers.length; i++) {
                        prototypes.add(MobPrototype.get(Integer.parseInt(numbers[i])));
                    }

                    wave.setPrototypes(spawnId, prototypes);

                } catch (RuntimeException e) {
                    throw new IllegalArgumentException("Bad wave settings, right format is \"!<int wave>,<int spawn>,<float rate>,<int mob>,...\", actual is:\n" + s, e);
                }

                continue;
            }

            // карта
            for (int x = 0; x < s.length(); x++) {

                IslandPoint point = new IslandPoint(x, y - skipped, s.charAt(x));

                point.ground = (point.symbol != WATER_SYMBOL && point.symbol != WATER_BRIDGE_SYMBOL && point.symbol != WATER_DECORATION_SYMBOL);

                if (point.symbol == PEPPERONI_SYMBOL) {
                    point.distanceToPepperoni = 0;
                    pepperonis.addLast(point);

                } else {
                    point.distanceToPepperoni = Integer.MAX_VALUE;
                }

                if (point.symbol == PATH_SYMBOL) point.pathType = IslandPathType.ROAD;

                if (point.symbol == WATER_BRIDGE_SYMBOL || point.symbol == GROUND_BRIDGE_SYMBOL)
                    point.pathType = IslandPathType.BRIDGE;

                insertPoint(point);
            }
        }

        return pepperonis;
    }

    /**
     * Метод 'калькулирует' стили.
     *
     * @see TileStyle
     */
    private void calculateStyle() {
        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {

                IslandPoint point = getPoint(x, y);
                if (point == null) continue;

                IslandPoint l = getPoint(x - 1, y);
                IslandPoint r = getPoint(x + 1, y);
                IslandPoint u = getPoint(x, y + 1);
                IslandPoint d = getPoint(x, y - 1);
                IslandPoint lu = getPoint(x - 1, y + 1);
                IslandPoint ru = getPoint(x + 1, y + 1);
                IslandPoint ld = getPoint(x - 1, y - 1);
                IslandPoint rd = getPoint(x + 1, y - 1);


                point.pathStyle = TileStyle.style4(
                        u != null && u.pathType == point.pathType,
                        d != null && d.pathType == point.pathType,
                        l != null && l.pathType == point.pathType,
                        r != null && r.pathType == point.pathType
                );

                point.groundStyle = TileStyle.style8(
                        u != null && u.ground == point.ground,
                        d != null && d.ground == point.ground,
                        l != null && l.ground == point.ground,
                        r != null && r.ground == point.ground,
                        lu != null && lu.ground == point.ground,
                        ru != null && ru.ground == point.ground,
                        ld != null && ld.ground == point.ground,
                        rd != null && rd.ground == point.ground
                );
            }
        }
    }

    /**
     * Посчитать значения для поиска путей.
     *
     * @param points изначальная коллекция со всеми пепперони на карте.
     */
    private void pathfinding(Queue<IslandPoint> points) {
        class CostUpdateUtil {
            void update(IslandPoint target, IslandPoint origin, Queue<IslandPoint> points) {
                if (target == null) return;
                if (target.pathType == IslandPathType.NONE) return;
                if (target.distanceToPepperoni != Integer.MAX_VALUE) return;

                target.distanceToPepperoni = origin.distanceToPepperoni + 1;
                points.addLast(target);
            }
        }

        CostUpdateUtil util = new CostUpdateUtil();

        while (points.size > 0) {
            IslandPoint poll = points.removeFirst();

            int x = poll.x;
            int y = poll.y;

            util.update(getPoint(x - 1, y), poll, points);
            util.update(getPoint(x + 1, y), poll, points);
            util.update(getPoint(x, y - 1), poll, points);
            util.update(getPoint(x, y + 1), poll, points);

        }
    }


    /**
     * Метод добавляет в игру все сущности которые зависят от карты.
     *
     * @param gameplay экран игры в который нужно добавить сущности.
     */
    public void setup(Gameplay gameplay) {
        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                IslandPoint point = getPoint(x, y);

                if (point == null) continue;

                if (point.symbol == PEPPERONI_SYMBOL) {
                    gameplay.getEntities().add(new Pepperoni(gameplay, x, y));
                }

                if (Character.isDigit(point.symbol)) {
                    gameplay.getEntities().add(new Spawn(gameplay, point.symbol - '0', x, y));
                }

                if (point.symbol == TOWER_SYMBOL) {
                    gameplay.getEntities().add(new TowerSpot(gameplay, x, y));
                }

                if (point.symbol == GROUND_DECORATION_SYMBOL) {
                    gameplay.getEntities().add(new Decoration(gameplay, x, y, true));
                }

                if (point.symbol == WATER_DECORATION_SYMBOL) {
                    gameplay.getEntities().add(new Decoration(gameplay, x, y, false));
                }
            }
        }
    }

    /**
     * Получить следующую точку наиболее близкую к пепперони.
     *
     * @param x x текущей точки.
     * @param y y текущей точки
     * @return следующая точка наиболее близкая к пепперони после {@code (x, y)}.
     */
    public IslandPoint navigateToPepperoni(int x, int y) {
        IslandPoint l = getPoint(x - 1, y);
        IslandPoint r = getPoint(x + 1, y);
        IslandPoint u = getPoint(x, y - 1);
        IslandPoint d = getPoint(x, y + 1);

        IslandPoint result;

        if (l == null || r != null && l.distanceToPepperoni > r.distanceToPepperoni) {
            result = r;
        } else {
            result = l;
        }

        if (result == null || u != null && result.distanceToPepperoni > u.distanceToPepperoni) {
            result = u;
        }

        if (result == null || d != null && result.distanceToPepperoni > d.distanceToPepperoni) {
            result = d;
        }

        return result;
    }

    /**
     * Метод вставляет точку в 'матрицу' {@link #tiles}.
     *
     * @param point точка которую нужно вставить.
     */
    private void insertPoint(IslandPoint point) {

        while (tiles.size <= point.x) {
            tiles.add(new Array<>());
        }

        Array<IslandPoint> column = tiles.get(point.x);

        while (column.size <= point.y) {
            column.add(null);
        }

        width = Math.max(width, point.x + 1);
        height = Math.max(height, point.y + 1);

        column.insert(point.y, point);
    }

    /**
     * Получить точку {@code (x, y)}.
     *
     * @param x x точки.
     * @param y y точки
     * @return точка с координатами {@code (x, y)}, если такой точки не существует, то {@code null}.
     */
    public IslandPoint getPoint(int x, int y) {
        if (x < 0 || x >= tiles.size || y < 0) return null;

        Array<IslandPoint> points = tiles.get(x);

        if (y >= points.size) return null;

        return points.get(y);
    }

    /**
     * Получить волну с определенным порядковым номером.
     *
     * @param i порядковый номер волны начиная с нуля.
     * @return волна с порядковым номером {@code i}, либо {@code null} если такой волны не существует.
     */
    public Wave getWave(int i) {
        return i < 0 || i >= waves.size
                ? null
                : waves.get(i);
    }

    /**
     * Получить кол-во волн на этой карте.
     */
    public int getWavesAmount() {
        return waves.size;
    }
}
