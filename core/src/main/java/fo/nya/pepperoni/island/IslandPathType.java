package fo.nya.pepperoni.island;

/**
 * Тип дорожки на острове по которым ходят враги.
 */
public enum IslandPathType {

    /**
     * Нет дороги.
     */
    NONE,

    /**
     * Дорога по земле.
     */
    ROAD,

    /**
     * Дорога в виде моста над рекой.
     */
    BRIDGE,
}
