package fo.nya.pepperoni;

import com.badlogic.gdx.utils.Array;


/**
 * Прототип врага, отвечает за внешний вид, скорость, здоровье и т.д.
 */
public enum MobPrototype {

    /**
     * Серый кот.
     */
    GRAY(1, 5, 0.28f, "gray_cat_down", "gray_cat_up", "gray_cat_left", "gray_cat_right"),

    /**
     * Рыжий кот.
     */
    GINGER(1.5f, 3, 0.24f, "ginger_cat_down", "ginger_cat_up", "ginger_cat_left", "ginger_cat_right"),

    /**
     * Лиловый кот.
     */
    PURPLE(3, 2, 0.14f, "purple_cat_down", "purple_cat_up", "purple_cat_left", "purple_cat_right"),

    ;

    /**
     * Список всех существующих 'прототипов'.
     */
    private final static Array<MobPrototype> cache = new Array<>(values());

    /**
     * Скорость врага.
     */
    public final float speed;

    /**
     * Изначально количество здоровья у врага.
     */
    public final int health;

    /**
     * Скорость анимации.
     */
    public final float animationSpeed;

    /**
     * Название анимации перемещения вниз.
     */
    public final String animationNameDown;
    /**
     * Название анимации перемещения вверх.
     */
    public final String animationNameUp;

    /**
     * Название анимации перемещения влево.
     */
    public final String animationNameLeft;

    /**
     * Название анимации перемещения вправо.
     */
    public final String animationNameRight;

    MobPrototype(float speed, int health, float animationSpeed, String animationNameDown, String animationNameUp, String animationNameLeft, String animationNameRight) {
        this.speed = speed;
        this.health = health;
        this.animationSpeed = animationSpeed;
        this.animationNameDown = animationNameDown;
        this.animationNameUp = animationNameUp;
        this.animationNameLeft = animationNameLeft;
        this.animationNameRight = animationNameRight;
    }

    /**
     * Получить случайный элемент типа {@link MobPrototype}
     */
    public static MobPrototype random() {
        return cache.random();
    }

    /**
     * Метод возвращает {@code enum} с {@code Enum.ordinal() == id} без создания новых массивов.
     *
     * @param ordinal порядковый номер значения.
     * @return {@code enum} с {@code Enum.ordinal() == id}, иначе {@code null} если нет такого {@code enum}.
     */
    public static MobPrototype get(int ordinal) {
        return ordinal >= 0 && ordinal < cache.size ? cache.get(ordinal) : null;
    }
}
