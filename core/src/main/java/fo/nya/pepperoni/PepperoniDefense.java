package fo.nya.pepperoni;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.FPSLogger;
import fo.nya.pepperoni.island.Island;
import fo.nya.pepperoni.island.MockIsland;

/**
 * Стартовый класс игры.
 */
public class PepperoniDefense extends Game {

    /**
     * Карта.
     */
    private final Island island = new Island(MockIsland.MAP);

    /**
     * Все ресурсы.
     */
    private AssetManager assets;

    /**
     * Текущий 'игровой процесс'.
     */
    private Gameplay gameplay;

    private final FPSLogger fps = new FPSLogger();

    @Override public void create() {
        assets = new AssetManager();
        Assets.loadAllInto(assets);
    }

    /**
     * Метод сбрасывает состояние игры, просто пересоздав его.
     */
    public void reset() {

        if (gameplay != null) {
            gameplay.dispose();
        }

        gameplay = new Gameplay(this, assets, island);
        setScreen(gameplay);
    }

    @Override
    public void render() {
        if (assets.update(15)) {

            if (getScreen() == null) {
                reset();
            }

            super.render();
            fps.log();
        } else {
            System.out.println(assets.getProgress());
        }
    }

    @Override
    public void dispose() {
        assets.dispose();
        gameplay.dispose();
        super.dispose();
    }
}
