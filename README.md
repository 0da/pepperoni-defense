Простой Tower Defense написанный на java с использованием libGDX и капелькой lombok'а.

- [lwjgl2 build](bin%2FPepperoniDefense-1.0.0-lwjgl2.jar)
- [lwjgl3 build](bin%2FPepperoniDefense-1.0.0-lwjgl3.jar)
- Online [pepperoni-defense.nya.fo](https://pepperoni-defense.nya.fo)

![game.png](bin%2Fgame.png)

---

Присутствует три абсолютно разных врага:
![ginger.png](bin%2Fginger.png)
![gray.png](bin%2Fgray.png)
![purple.png](bin%2Fpurple.png)

И одна башенка:
![jar.png](bin%2Fjar.png)

---

Загрузка ресурсов через AssetManager. UI через Scene2D, сообщение о поражении и победе, пауза, рестарт. Враги условно
случайны, это можно сделать легким движением руки, но сейчас они "сохранены как часть карты".

За основу для арта взят арт отсюда и переделан под котов:
[cupnooble.itch.io](https://cupnooble.itch.io)

Колбаса напоследок.
![pepperoni.png](bin%2Fpepperoni.png)
